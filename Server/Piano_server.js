const express = require('express');
const app   = express();
const http  = require('http').Server(app);
const io    = require('socket.io')(http, { pingTimeout: 60000 });
const path  = require('path');

// Initialise file path for the client resources. 
var clientRoot = path.resolve(__dirname, '../Client/');
app.use(express.static(clientRoot));

// Return the client when server URL is requested
app.get('/', function(request, response){
    response.sendFile('Piano.html', {root: clientRoot});
});

// Handle connection
io.on('connection', function(socket){

    // Client sent username event, broadcast to other clients
    socket.on('username', function(username){
        socket.username = username;
        socket.broadcast.emit('userJoin', username + ' has joined the room.');
    });

    // Client disconnected, broadcast to other clients
    socket.on('disconnect', function(){
        if (socket.username) socket.broadcast.emit('userLeave', socket.username + ' has left the room.');
    });

    // Client sent chat event, broadcast to other clients
    socket.on('chat', function(msg){
        socket.broadcast.emit('chat', { sender: socket.username, message: msg });
    });

    // Client sent key press event, broadcast to other clients
    socket.on('keyPress', function(key){
        socket.broadcast.emit('keyPress', key);
    });

    // Client sent key press event, broadcast to other clients
    socket.on('keyUp', function() {
        socket.broadcast.emit('keyUp');
    });
});

// Listen on port 9000
http.listen(9000, function(){
    console.log('.. Listening on port 9000');
});