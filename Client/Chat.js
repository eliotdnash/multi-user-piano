function ShowUserNameModal(){
    // Show the username modal and blur background
    $('#username-modal').show();
    $('.container-fluid').addClass('blur');
}

// Add a message to the chat window - general purpose
function AppendMessageToChat(msg, messageType) {
    // Check if the chat window is scrolled to the bottom
    var atBottom = IsChatScrolledToBottom();
    var chat = $('#messages');

    // Handle different types of messages and add different css classes
    switch (messageType) {
        // Client has connected/disconnected
        case 'status':
            chat.append($('<li class="chat-user-status">').text(msg));
            break;
        // Chat message has been received
        case 'received':
            chat.append($('<li>').text(msg));
            break;
        // We sent a chat message
        case 'sent':
            chat.append($('<li class="sent-message">').text(msg));
            break;
        default:
            break;
    }

    // React to the new message
    HandleNewMessage(atBottom);
}

// Check if the chat window is scrolled all the way to the bottom
function IsChatScrolledToBottom(){
    var chatBox = $('#msg-history');

    if (chatBox.scrollTop() + chatBox.innerHeight() >= chatBox[0].scrollHeight - 40) {
        return true;
    }    
}

function HandleNewMessage(atBottom) {
    // "Sticky" new message. If we're scrolled to the bottom, stay scrolled to the bottom
    if (atBottom) {
        ScrollToBottom();
    } else {
        // If we're not at the bottom, show us a button to inform that there are new messages
        $('.new-message').show();
    }
}

// Scroll the chat window all the way to the bottom
function ScrollToBottom(){
    var chatBox = $('#msg-history');

    chatBox.scrollTop(chatBox[0].scrollHeight);
}