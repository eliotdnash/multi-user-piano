var middleCBuffer = null;
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioCtxt = new AudioContext();
var whiteKeyIds = [];
var blackKeyIds = [];
var qwertyKeyCodes =   [49, 50, 51, 52, 53, 54, 55, 56, 57, 48, // 1,2,3,4,5,6,7,8,9,0
                        81, 87, 69, 82, 84, 89, 85, 73, 79, 80, //  q,w,e,r,t,y,u,i,o,p
                        65, 83, 68, 70, 71, 72, 74, 75, 76,     //   a,s,d,f,g,h,j,k,l
                        90, 88, 67, 86, 66, 78, 77];            //    z,x,c,v,b,n,m

// When the document loads, register events and prepare the piano for playing.
$(document).ready(function() {
    // Get lists of our key div ids
    whiteKeyIds = MapPianoKeyIds(".white");
    blackKeyIds = MapPianoKeyIds(".black");

    RegisterEvents(); // Events.js <- RegisterEvents()

    LoadMiddleCAudio(); // Load our audio file.

    ShowUserNameModal(); // Prompt the user to add a username and disable other functionality
});

// Load our MP3 file into an array buffer.
function LoadMiddleCAudio() {
    var middleCUrl = './resources/C4.mp3';

    fetch(middleCUrl) // Attempt to fetch mp3 from resources,
    .then(response => response.arrayBuffer()) // When fetch promise returns, convert response to an array buffer.
    .then(buffer => {                         // When promise returns, attempt to decode array buffer into an audio buffer.
        audioCtxt.decodeAudioData(buffer, decodedBuffer => {
            middleCBuffer = decodedBuffer;
        }, error => {
            $('#error').show();
        });
    });
}

// Play the audio for the selected key.
function PlayKey(keyNumber) {
    // Determine the speed to playback our audio file.
    var rate = GetPlaybackRate(keyNumber);

    // Create our audio source.
    var src = audioCtxt.createBufferSource();
    var gainCtxt = audioCtxt.createGain();

    src.buffer = middleCBuffer;
    // Set the buffer's playback rate.
    src.playbackRate.setValueAtTime(rate, 0);
    src.connect(gainCtxt); 
    gainCtxt.connect(audioCtxt.destination);

    // Play the audio from the beginning.
    src.start(0);

    // Reduce gain/volume over time to prevent annoying sustain.
    gainCtxt.gain.exponentialRampToValueAtTime(0.001, audioCtxt.currentTime + 2.5);
    src.stop(audioCtxt.currentTime + 2.5);
}

// Calculate the rate to play our audio file.
// Each octave has 12 semitones.
function GetPlaybackRate(keyNumber) {
    return Math.pow(2, (keyNumber / 12));
}

// Used to get lists of white key (div) ids and black key (div) ids.
function MapPianoKeyIds(keyType) {
    return $.map($('#keys ' + keyType), function(key, i){
        return key.id;
    });
}