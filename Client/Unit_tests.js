// Style/Structure unit tests

QUnit.test('Test 1: Testing page title', function( assert ) {
    assert.equal($('title').text(), 'Multi-user Piano', 'Page title is "Multi-user Piano"' );
});

QUnit.test('Test 2a: Checking for heading', function( assert ) {
    assert.ok($('h1').length > 1, 'Heading is present' );
});

QUnit.test('Test 2b: Test heading', function( assert ) {
    assert.equal($('.page-header h1').text(), 'Multi-user Piano', 'Heading is "Multi-user Piano"' );
});

QUnit.test('Test 3a: Checking for piano div', function( assert ) {
    assert.ok($('div#piano').length == 1, 'Piano div exists' );
});

QUnit.test('Test 4a: Testing number of keys', function( assert ) {
    assert.equal($('#keys > div').length, 61, 'Piano has 61 keys');
});

QUnit.test('Test 4b: Testing number of white keys', function( assert ) {
    assert.equal($('.white').length, 36, 'Piano has 36 white keys');
});

QUnit.test('Test 4c: Testing number of black keys', function( assert ) {
    assert.equal($('.black').length, 25, 'Piano has 25 black keys');
});

QUnit.test('Test 4d: Testing overall key width', function ( assert ) {
    assert.equal(Math.round($('#keys').width()), 
                    Math.round($('#piano').width()),
                    'Keys are the same width as the piano'
                );
});

QUnit.test('Testing 4e: Testing piano colour', function ( assert ){
    assert.equal($('#piano').css('background-image'),
                'linear-gradient(rgb(0, 0, 0) 20%, rgb(70, 70, 70) 85%, rgb(56, 56, 56) 86%, rgb(0, 0, 0) 98%)',
                'Piano background style is: linear-gradient(rgb(0, 0, 0) 20%, rgb(70, 70, 70) 85%, rgb(56, 56, 56) 86%, rgb(0, 0, 0) 98%)');
});

QUnit.test('Testing 4f: Testing white key colour', function ( assert ) {
    assert.equal($('.white').css('background-image'),
                'linear-gradient(rgb(255, 255, 255) 20%, rgb(247, 247, 247) 88%, rgb(197, 197, 197) 90%, rgb(255, 255, 255) 98%)' ,
                'TWhite key background style is: linear-gradient(rgb(255, 255, 255) 20%, rgb(247, 247, 247) 88%, rgb(197, 197, 197) 90%, rgb(255, 255, 255) 98%)');
});

QUnit.test('Test 4g: Testing black key colour', function ( assert ) {
    assert.equal($('.black').css('background-image'),
                'linear-gradient(rgb(0, 0, 0) 20%, rgb(93, 92, 92) 88%, rgb(72, 72, 72) 90%, rgb(0, 0, 0) 98%)' ,
                'Black key background style is: linear-gradient(rgb(0, 0, 0) 20%, rgb(93, 92, 92) 88%, rgb(72, 72, 72) 90%, rgb(0, 0, 0) 98%)');
});

QUnit.test('Test 5a: Checking for chat history', function( assert ) {
    assert.ok($('div#msg-history').length == 1, 'Chat history div exists' );
});

QUnit.test('Test 5b: Checking chat history background colour', function( assert ) {
    assert.equal($('div#msg-history').css('background-color'),'rgb(43, 40, 40)', 'Chat background colour is: rgb(43, 40, 40)' );
});

QUnit.test('Test 5c: Checking received chat font colour', function( assert ) {
    assert.equal($('#messages').css('color'),'rgb(255, 192, 203)', 'Received chat font colour is: rgb(255, 192, 203)' );
});

QUnit.test('Test 5d: Checking send chat button text', function( assert ) {
    assert.equal($('#send-message').text(),'Send Message', 'Send chat button text is: "Send Message"' );
});

// Functionality unit tests.

QUnit.test('Test 6a: Testing white key pressed styling', function ( assert ){
    $('.white').addClass('mouse-down');

    assert.equal($('.white').css('background-image'),
                'linear-gradient(rgb(212, 212, 212) 20%, rgb(196, 196, 196) 93%, rgb(158, 158, 158) 95%, rgb(212, 212, 212) 99%)',
                'Pressed white key background is:  linear-gradient(rgb(212, 212, 212) 20%, rgb(196, 196, 196) 93%, rgb(158, 158, 158) 95%, rgb(212, 212, 212) 99%)');

    $('.white').removeClass('mouse-down');
});

QUnit.test('Test 6b: Testing black key pressed styling', function ( assert ){
    $('.black').addClass('mouse-down');

    assert.equal($('.black').css('background-image'),
                'linear-gradient(rgb(73, 73, 73) 20%, rgb(105, 105, 105) 93%, rgb(88, 88, 88) 95%, rgb(73, 73, 73) 99%)',
                'Pressed white key background is: linear-gradient(rgb(73, 73, 73) 20%, rgb(105, 105, 105) 93%, rgb(88, 88, 88) 95%, rgb(73, 73, 73) 99%)');

    $('.black').removeClass('mouse-down');
});

QUnit.test('Test 7: Testing playback rate calculation', function ( assert ){
    assert.expect(61);
    var keys = $('#keys div');

    for (var i = 0; i < keys.length; i++) {
        var keyNumber = keys[i].id;
        assert.equal(GetPlaybackRate(keyNumber), Math.pow(2, (keyNumber / 12)), 'Playback rate for key id: ' + keyNumber + ' is correct');
    }
});

QUnit.test('Test 8: Testing playback rate calculation', function ( assert ){
    assert.expect(61);
    var keys = $('#keys div');

    for (var i = 0; i < keys.length; i++) {
        var keyNumber = keys[i].id;
        assert.equal(GetPlaybackRate(keyNumber), Math.pow(2, (keyNumber / 12)), 'Playback rate for key id: ' + keyNumber + ' is correct');
    }
});

QUnit.test('Test 9a: Testing key mapping function (white)', function (assert ){
    var numberOfKeys = $('#keys .white').length;
    var keyArray = MapPianoKeyIds('.white');

    assert.equal(keyArray.length, numberOfKeys, 'Key map function returns the correct number of keys (white)');
});

QUnit.test('Test 9b: Testing key mapping function (black)', function (assert ){
    var numberOfKeys = $('#keys .black').length;
    var keyArray = MapPianoKeyIds('.black');

    assert.equal(keyArray.length, numberOfKeys, 'Key map function returns the correct number of keys (black)');
});

QUnit.test('Test 10a: Testing GetPianoKey function (keydown event, username modal hidden)', function ( assert ){
    assert.expect(2);

    $('#username-modal').hide();

    var mockEvent = new Event('keydown');
    mockEvent.which = mockEvent.keyCode = 69; // e key <- should return keyNumber -3

    assert.equal(GetPianoKey(mockEvent), -3, 'GetPianoKey function returned the correct key id: e -> -3');

    mockEvent.which = 84 // t key <- should return keyNumber 0 (middle C)

    assert.equal(GetPianoKey(mockEvent), 0, 'GetPianoKey function returned the correct key id: t -> 0');

    $('#username-modal').show();
})

QUnit.test('Test 10b: Testing GetPianoKey function (keyup event, username modal hidden)', function ( assert ){
    assert.expect(2);

    $('#username-modal').hide();
    
    var mockEvent = new Event('keyup');
    mockEvent.which = mockEvent.keyCode = 49; // 1 key <- should return keyNumber -24

    assert.equal(GetPianoKey(mockEvent), -24, 'GetPianoKey function returned the correct key id: 1 -> -24');

    mockEvent.which = mockEvent.keyCode = 77 // m key <- should return keyNumber 36

    assert.equal(GetPianoKey(mockEvent), 36, 'GetPianoKey function returned the correct key id: m -> 36');

    $('#username-modal').show();
})

QUnit.test('Test 10c: Testing GetPianoKey function (username modal visible)', function( assert ) {
    var mockEvent = new Event('keydown');
    mockEvent.which = mockEvent.keyCode = 49; // 1 key <- should return keyNumber -24 unless username modal visible

    assert.equal(GetPianoKey(mockEvent), undefined, 'GetPianoKey function returned nothing because username modal was visible');
})

QUnit.test('Test 10d: Testing GetPianoKey function (shift key pressed)', function( assert ) {
    var mockEvent = new Event('keydown');
    mockEvent.which = mockEvent.keyCode = 16; // 1 key <- should return keyNumber -24 unless username modal visible

    $('#username-modal').hide(); // Rules out username modal causing null return;
    assert.equal(GetPianoKey(mockEvent), undefined, 'GetPianoKey function returned nothing because shift key pressed');
    $('#username-modal').show();
})