var socket = io(); // Instantiate socket-client

/*** Socket Events Received ***/
    // Handle a user joining the server 
    socket.on('userJoin', function(msg){
        AppendMessageToChat(msg, 'status');
    });

    // Handle a user leaving the server
    socket.on('userLeave', function(msg){
        AppendMessageToChat(msg, 'status');
    });

    // Handle a message being received
    socket.on('chat', function(chatData){   
        var user = chatData.sender;
        var msg = user + ': ' + chatData.message;

        AppendMessageToChat(msg, 'received');
    });

    // Handle a piano key-press event from another client
    socket.on('keyPress', function(keyNumber){
        var keyPressed = $('#' + keyNumber);

        ClickKey(keyPressed, false);
    });

    // Handle a piano key-up event from another client
    socket.on('keyUp', function(){
        $('#keys div').removeClass('mouse-down');
    });
/******/

/*** Socket Events Sent ***/
    // Send the client's username to other clients
    function SendUsername(username) {
        socket.emit('username', username);
    }

    // Send a chat message to other clients
    function SendMessage(msg) {
        socket.emit('chat', msg);
    }

    // Send a piano key-press event to other clients
    function SendKeyPressEvent(keyNumber) {
        socket.emit('keyPress', keyNumber);
    }

    // Send a piano key-up event to other clients
    function SendKeyUpEvent() {
        socket.emit('keyUp');
    }
/******/