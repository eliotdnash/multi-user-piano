function RegisterEvents() {
    // Is the mouse being clicked?
    var mouseDown = false;

    $('#toggle-tests').on('click', function(){
        $('#test-fixture').toggle();
    });

    // Mouse down event.
    $('#keys div').mousedown(function(e){
        mouseDown = true;

        ClickKey($(this), true);        
    });

    // Mouse up event.
    $(document).mouseup(function(e) {
        mouseDown = false;

        SendKeyUpEvent(); // Use socket connection to send the event to other clients

        $('#keys div').removeClass('mouse-down');
    });

    // Mouse drag/hover event.
    $('#keys div').mouseenter(function(){
        // If the mouse button is still held down, play the key being hovered over.
        if (mouseDown) {
            ClickKey($(this), true);
        }
    });

    // Computer key down event.
    $(document).keydown(function(e){
        // Translate computer key to piano key number.
        var keyNumber = GetPianoKey(e);

        // Get the corresponding piano key element.
        var keyPressed = $('#' + keyNumber);

        // If no corresponding piano key, don't perform any action.
        if (keyPressed.length < 1) return;

        // If the key isn't already being held down, play the note.
        if (!keyPressed.hasClass('mouse-down')){
            keyPressed.addClass('mouse-down');

            SendKeyPressEvent(keyNumber); // Use socket connection to send the event to other clients

            PlayKey(keyNumber);
        }
    });

    // Computer key up event.
    $(document).keyup(function(e) {
        // Translate unpressed computer key to piano key element.
        var keyNumber = GetPianoKey(e);
        var keyUnpressed = $('#' + keyNumber);

        // If no corresponding piano key, don't perform any action.
        if (keyUnpressed.length < 1) return;

        SendKeyUpEvent(keyNumber); // Use socket connection to send the event to other clients

        // Remove the "pressed" styling from the piano key.
        keyUnpressed.removeClass('mouse-down');
    });

    // If the enter key is pressed when the user types their message
    $('#input-box').keypress(function(e){
        var keycode = e.which || e.keyCode;

        if(keycode == '13'){
            // Send the message
            $('#send-message').click();
        }
    });

    // Click event for the send message button
    $('#send-message').click(function(){
        var input = $('#input-box');
        var warningText = $('.text-danger');
        var msg = input.val(); // Get input

        if (msg.length <= 250) {
            SendMessage(msg); // Use socket connection to send the message to other clients
            AppendMessageToChat(msg, 'sent'); // Add the message to chat

            input.removeClass('is-invalid');
            warningText.hide();
            input.val(''); // Clear input box
        } else {
            warningText.show();
            input.addClass('is-invalid');
        }
    });

    // Scroll event for the chat window
    $('#msg-history').on('scroll', function(){
        var atBottom = IsChatScrolledToBottom(); // Check if the chat is scrolled down fully

        if (atBottom) {
            $('.new-message').hide(); // Hide the new message (notifier) button
        }
    });

    // Click event for the new message (notifier) button
    $('.new-message').on('click', function(){
        ScrollToBottom();
    });

    // If the enter key is pressed when the user enters their username
    $('#username-box').keypress(function(e){
        var keycode = e.which || e.keyCode;

        if(keycode == '13'){
            // Register the username
            $('#register-username').click();
        }
    });

    // Click event for the register username button
    $('#register-username').on('click', function() {
        var input = $('#username-box');
        var clientUsername = input.val(); // Get the input username

        if (clientUsername == "") {
            input.addClass('is-invalid'); // If the field is unpopulated, do not send
        } else {
            SendUsername(clientUsername); // Use socket connection to send to the other clients
            // Hide the modal and make the rest of the page visible
            $('#username-modal').hide();
            $('.container-fluid').removeClass('blur');
        }
    });
}

// Perform click action on the selected piano key.
function ClickKey(keyPressed, sendClick) {
    // Get the piano key number.
    var keyNumber = keyPressed.attr('id');

    if (sendClick) SendKeyPressEvent(keyNumber); // Use socket connection to send the keypress to other clients

    // Add "pressed" styling to the selected key.
    keyPressed.addClass('mouse-down');

    // Play the corresponding audio for the key.
    PlayKey(keyNumber);
}

// Translates computer keys to our virtual piano keys.
function GetPianoKey(event) {
    // Determine which computer key has been pressed/unpressed.
    var key = event.which || event.keyCode;

    // Don't do anything if the user hasn't entered a username, is typing in the chat box, or has used the shift key.
    if ($('#username-modal').is(':visible') || $('#input-box').is(':focus') || key == 16) return;

    // Get the index of our computer key (0-60).
    // This index is used in our piano key arrays to find the corresponding piano key.
    var keyIndex = qwertyKeyCodes.indexOf(key);
    var keyNumber = "";

    // If the shift key is being held down, play a black key.
    if (event.shiftKey) {
        // Find the id of the black key to be played.
        keyNumber = blackKeyIds[keyIndex];
    }
    else {
        // Find the id of the white key to be played.
        keyNumber = whiteKeyIds[keyIndex];
    }

    return keyNumber;
}